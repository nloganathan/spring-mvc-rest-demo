
## A simple REST web service implemented with Spring MVC.

### The use case is create a simple user management and authentication system.

#### Requirements:
	- Create, Update, Delete, List web service endpoints for a User object
	- User object should contain a name, email address, password and the date of their last login
	- Provide a login endpoint that validates the email address and password provided by the user matches the one stored in the database

### Technology stack:

 - Spring Boot 2.1.1
 - Spring data 2.1.3
 - Spring web 5.1.3
 - JPA 2.2
 - Hibernate 5.3.7
 - Apache Derby 10.14
 - Tomcat Embedded 9.0.13
 - Java 8

### How to run?

 1. Clone the spring-mvc-rest-demo repository
	- git clone https://gitlab.com/liveearth/spring-mvc-rest-demo.git
 2. From within the spring-mvc-rest-demo/ directory, run `mvn clean install` to run build and tests.
	- cd spring-mvc-rest-demo/
	- mvn clean install
 3. Run the below command to start the embedded tomcat
	- java -jar target/spring-mvc-rest-demo-1.0.0-SNAPSHOT.jar
 4. Make API requests using cURL or any HTTP client tool as shown in the below examples.

### Example REST requests:

 - Create a User:
	`curl -X POST http://localhost:8080/users -H 'Content-Type: application/json' -d '{"email":"testuser@test.com","password":"test1234","name": "testuser"}'`
 - List Users:
	`curl -X GET http://localhost:8080/users`
 - Update a User:
	`curl -X PUT http://localhost:8080/users -H 'Content-Type: application/json' -d '{"email":"testuser@test.com","password":"test@1234","name": "testuser"}'`
 - Login as User:
	`curl -X POST http://localhost:8080/login -H 'Content-Type: application/json' -d '{"email":"testuser@test.com","password":"test@1234"}'`
 - Delete a User:
	`curl -X DELETE http://localhost:8080/users/{email}`
	`curl -X DELETE http://localhost:8080/users/testuser@test.com`



