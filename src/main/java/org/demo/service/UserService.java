package org.demo.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.demo.controller.dto.UserDTO;
import org.demo.data.UserRepository;
import org.demo.exception.UserNotFoundException;
import org.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);

		return users;
	}

	public User createUser(User user) {
		return userRepository.save(user);
	}

	public User getUserByEmailAndPassword(User user) {
		Optional<User> optionalUser = Optional
				.ofNullable(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword()));
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			throw new UserNotFoundException("Please check your email and password");
		}
	}

	public UserDTO loginUser(User user) {
		User lastLoginUser = getUserByEmailAndPassword(user);
		UserDTO userDTO = convertToDTO(lastLoginUser);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		lastLoginUser.setLastLogin(calendar.getTime());
		userRepository.save(lastLoginUser);
		return userDTO;
	}

	public User updateUser(User user) {
		if (userRepository.existsById(user.getEmail())) {
			return userRepository.save(user);
		} else {
			throw new UserNotFoundException("Given user: " + user.getEmail() + " not exist.");
		}
	}

	public void deleteUser(String email) {
		userRepository.deleteById(email);
	}

	private UserDTO convertToDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setName(user.getName());
		userDTO.setEmail(user.getEmail());
		userDTO.setPassword(user.getPassword());
		userDTO.setLastLogin(user.getLastLogin());
		return userDTO;
	}
}
